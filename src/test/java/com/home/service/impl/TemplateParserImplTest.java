package com.home.service.impl;

import com.home.RequestService;
import com.home.service.PythonService;
import com.home.service.TemplateParser;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RequestService.class)
public class TemplateParserImplTest {

    @Autowired
    private TemplateParser templateParser;

    @Autowired
    private PythonService converter;

    @Test
    public void parseTemplate() throws IOException {
        converter.startPythonConvertionService("C:\\projects\\hackaton\\src\\main\\resources\\python\\converet_to_pdf.py",
                "C:\\projects\\hackaton\\src\\main\\resources\\python\\out.pdf", "https://7e7c22fe.ngrok.io/test_template");
    }
}