package com.home.dao;

import com.botscrew.botframework.domain.user.Bot;
import com.home.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Long> {
    User findByChatIdAndBot(Long chatId, Bot bot);
}