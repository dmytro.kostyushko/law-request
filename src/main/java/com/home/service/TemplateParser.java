package com.home.service;

import java.util.Map;

public interface TemplateParser {

    String parseTemplate(String templateLocation, Map<String, Object> properties);
}
