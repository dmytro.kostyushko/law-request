package com.home.service.impl;

import com.home.service.TemplateParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;
import java.util.Map;

@Service
@Log4j2
@RequiredArgsConstructor
public class TemplateParserImpl implements TemplateParser {

    private final TemplateEngine templateEngine;

    @Override
    public String parseTemplate(String templateLocation, Map<String, Object> properties) {
        final Context ctx = new Context(Locale.getDefault());
        for (String propertyName : properties.keySet()) {
            ctx.setVariable(propertyName, properties.get(propertyName));
        }
        return this.templateEngine.process(templateLocation, ctx);
    }

}
