package com.home.service.impl;

import com.home.exceptions.SystemException;
import com.home.service.PythonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class PythonServiceImpl implements PythonService {

    @Override
    public void startPythonConvertionService(String scriptLocation, String... args) {
        try {
            String format = "python %s %s";
            String commandArguments = Stream.of(args).collect(Collectors.joining(" "));
            String command = String.format(format, scriptLocation, commandArguments);
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            in.lines().forEach(System.out::println);
        } catch (IOException e) {
            throw new SystemException(e);
        }
    }

}
