package com.home.service.impl;

import com.botscrew.botframework.domain.user.Bot;
import com.botscrew.messengercdk.model.incomming.Profile;
import com.botscrew.messengercdk.service.Messenger;
import com.home.constant.PropertyKey;
import com.home.dao.UserDao;
import com.home.entity.User;
import com.home.property.Property;
import com.home.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserDao userDao;
    private final Messenger messenger;
    private final Property property;

    @Override
    public User findByChatIdAndBot(Long chatId, Bot bot) {
        return userDao.findByChatIdAndBot(chatId, bot);
    }

    @Override
    public User createUserIfExist(Long chatId){
       Profile userProfile =  messenger.getProfile(chatId,property.getStringProperty(PropertyKey.FACEBOOK_TOKEN));
       User user = new User();
       user.setFirstName(userProfile.getFirstName());
       user.setLastName(userProfile.getLastName());
       return this.save(user);
    }

    @Override
    public User save(User user){
        return userDao.save(user);
    }

}