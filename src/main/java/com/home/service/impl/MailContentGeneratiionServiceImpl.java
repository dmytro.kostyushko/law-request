package com.home.service.impl;

import com.home.service.MailContentGeneratiionService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MailContentGeneratiionServiceImpl implements MailContentGeneratiionService {

    @Override
    public String getMailImageContent() {
        return "<img src='https://7e7c22fe.ngrok.io/image/{{token}}/s'/>".replace("{{token}}", UUID.randomUUID().toString());
    }

}
