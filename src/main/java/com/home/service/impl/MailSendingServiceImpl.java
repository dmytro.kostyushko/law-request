package com.home.service.impl;

import com.home.exceptions.SystemException;
import com.home.service.MailSendingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Log4j2
@RequiredArgsConstructor
public class MailSendingServiceImpl implements MailSendingService {

    private final JavaMailSender emailSender;

    @Override
    @Async
    public void sendMail(String subject, String content, String destination) {
        log.info("Sending email to {}", destination);
        emailSender.send(getMail(content, destination, subject));
        log.info("Email sent successful", destination);
    }

    private MimeMessage getMail(String text, String receiverMail, String mailSubject) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, false, "utf-8");
            message.setContent(text, "text/html");
            messageHelper.setTo(receiverMail);
            messageHelper.setSubject(mailSubject);
            return message;
        } catch (MessagingException e) {
            throw new SystemException(e);
        }
    }

}
