package com.home.service.impl;

import com.home.service.SendMessageService;
import com.home.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
public class SendMessageServiceImpl implements SendMessageService {
    private final UserService userService;

}
