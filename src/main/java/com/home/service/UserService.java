package com.home.service;

import com.botscrew.botframework.domain.user.Bot;
import com.home.entity.User;

public interface UserService {
    User findByChatIdAndBot(Long chatId, Bot bot);

    User createUserIfExist(Long chatId);

    User save(User user);
}
