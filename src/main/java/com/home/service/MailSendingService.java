package com.home.service;

public interface MailSendingService {
    void sendMail(String subject, String content, String destination);
}
