package com.home.service;

public interface PythonService {
    void startPythonConvertionService(String scriptLocation, String... args);
}
