package com.home.constant;

public enum PropertyKey {
    FACEBOOK_TOKEN("facebook.token"),
    DIALOG_FLOW_CLIENT__TOKEN("dialog.flow.client.token")
    ;

    private String key;

    PropertyKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
