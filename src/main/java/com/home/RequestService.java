package com.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@PropertySource("classpath:custom.properties")
@SpringBootApplication()
public class RequestService {


    public static void main(String[] args) {
        SpringApplication.run(RequestService.class, args);
    }


}
