package com.home.controller;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;

@Log4j2
@RestController
public class ImageController {


    @GetMapping("/image/{token}/{secret}")
    public void getImage(@PathParam("token") String token,
                         @PathParam("secret") String secret,
                         HttpServletResponse response) throws IOException {
        log.info("Request for image with token {} and secret {}", token, secret);
        ClassPathResource resource = new ClassPathResource("/test_logo.png");
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(resource.getInputStream(), response.getOutputStream());
    }

}
