package com.home.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DocumentController {



    @RequestMapping("/test_template")
    public String returnTestTemplate(Model model) {
        model.addAttribute("address", "address");
        model.addAttribute("phone", "phone");
        model.addAttribute("receiver", "receiver");
        model.addAttribute("sender", "sender");
        return "test_template";
    }


}
