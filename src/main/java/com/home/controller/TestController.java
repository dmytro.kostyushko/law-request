package com.home.controller;

import com.home.service.PythonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final PythonService pythonService;

    @GetMapping("/generate")
    public void convertTemplateIntoPdf() {
        String pythonScriptLocation = "C:\\projects\\hackaton\\src\\main\\resources\\python\\converet_to_pdf.py";
        String outputLocation = "C:\\projects\\hackaton\\src\\main\\resources\\python\\out.pdf";
        String templateUrl = "https://7e7c22fe.ngrok.io/test_template";

        pythonService.startPythonConvertionService(
                pythonScriptLocation,
                outputLocation,
                templateUrl);
    }

}
