package com.home.configuration.init;

import com.botscrew.messengercdk.model.outgoing.element.button.GetStartedButton;
import com.botscrew.messengercdk.service.Messenger;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class GetStartedButtonInit {
    private final Messenger messenger;

    @PostConstruct
    public void initGetStartedButton() {
        messenger.setGetStartedButton(new GetStartedButton("GET_STARTED"));
    }
}
