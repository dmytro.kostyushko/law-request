package com.home.configuration.init;

import com.botscrew.messengercdk.model.outgoing.element.WebHook;
import com.botscrew.messengercdk.service.Messenger;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Log4j2
@Component
@AllArgsConstructor
public class WebhookInit {
    private final Messenger messenger;

    public void updateWebhook() {
        log.info("Try update Facebook webhook.");
        String webhookUrl = "https://7e7c22fe.ngrok.io";
        log.info("Update webhook. Webhook url is: " + webhookUrl);
        messenger.setWebHook(webhookUrl + "/messenger/events",
                Arrays.asList(
                        WebHook.Field.MESSAGES,
                        WebHook.Field.POSTBACKS,
                        WebHook.Field.ECHOES
                ));
    }
}