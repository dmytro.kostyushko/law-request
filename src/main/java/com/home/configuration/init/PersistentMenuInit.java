package com.home.configuration.init;

import com.botscrew.messengercdk.model.outgoing.profile.menu.NestedMenuItem;
import com.botscrew.messengercdk.model.outgoing.profile.menu.PersistentMenu;
import com.botscrew.messengercdk.model.outgoing.profile.menu.PostbackMenuItem;
import com.botscrew.messengercdk.model.outgoing.profile.menu.WebMenuItem;
import com.botscrew.messengercdk.service.Messenger;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Component
@AllArgsConstructor
public class PersistentMenuInit {
    private final Messenger messenger;

    @PostConstruct
    public void persistentMenuInit() {
        PersistentMenu menu = new PersistentMenu(
                Arrays.asList(
                        new PostbackMenuItem("Postback", "MENU_POSTBACK"),
                        new WebMenuItem("Test Web URL", "https://google.com"),
                        NestedMenuItem.builder()
                                .title("Nested")
                                .addMenuItem(PostbackMenuItem.builder()
                                        .title("Test Postback")
                                        .payload("PAYLOAD")
                                        .build())
                                .build()
                )
        );
        messenger.setPersistentMenu(menu);
    }
}
