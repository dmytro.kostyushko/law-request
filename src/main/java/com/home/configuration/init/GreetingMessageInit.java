package com.home.configuration.init;

import com.botscrew.messengercdk.model.outgoing.profile.Greeting;
import com.botscrew.messengercdk.service.Messenger;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class GreetingMessageInit {
    private final Messenger messenger;

    @PostConstruct
    public void greetingMessageInit(){
        messenger.setGreeting(new Greeting("HI there bro!"));
    }
}
