package com.home.flow;

import ai.api.model.AIResponse;
import com.botscrew.botframework.annotation.*;
import com.botscrew.botframework.sender.Sender;
import com.botscrew.messengercdk.model.incomming.Message;
import com.botscrew.messengercdk.model.outgoing.request.*;
import com.botscrew.nlpclient.domain.DialogFlowV1Credentials;
import com.botscrew.nlpclient.provider.NlpClient;
import com.home.constant.PropertyKey;
import com.home.entity.User;
import com.home.property.Property;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;

@Log4j2
@IntentProcessor
@ChatEventsProcessor
@AllArgsConstructor
public class DefaultFlow {
    private final NlpClient nlpClient;
    private final Property property;
    private final Sender sender;


    @Postback
    @Referral
    public void defaultProcessPostback(User user, @Postback @Referral String atomName) {
        log.info("Process postabck without state. Message [ " + atomName + " ]" + " for user: " + user.getFullNameAndChatId());
        //TODO
    }

    @Text
    public void defaultProcessText(User user, @Text String message) {
        log.info("Process message without state. Message [ " + message + " ]" + " for user: " + user.getFullNameAndChatId());
        nlpClient.query(user, message, user.getChatId().toString(), new DialogFlowV1Credentials(property.getStringProperty(PropertyKey.DIALOG_FLOW_CLIENT__TOKEN)));
    }

    @Echo
    public void defaultEcho(User user, @Original Message message) {
        //TODO nothing
    }

    @Intent
    public void handleDefaultIntent(final User user, @Original AIResponse aiResponse, @Intent String intentName) {
        log.info("Process intent without state for user: " + user.getFullNameAndChatId() + ". Incoming intent name is: " + intentName);
    }
}