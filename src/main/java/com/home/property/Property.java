package com.home.property;

import com.home.constant.PropertyKey;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class Property {
    private final Environment environment;

    public String getStringProperty(PropertyKey propertyKey){
        return environment.getProperty(propertyKey.getKey());
    }
}
