package com.home.entity;

import com.botscrew.botframework.domain.user.Platform;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Bot implements com.botscrew.botframework.domain.user.Bot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Override
    public Platform getPlatform() {
        return Platform.FB_MESSENGER;
    }
}
