package com.home.entity;

import com.botscrew.messengercdk.model.MessengerUser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class User implements MessengerUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long chatId;
    private String state;
    private String firstName;
    private String lastName;
    @ManyToOne
    @JoinColumn(name = "bot_id")
    private Bot bot;

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getFullNameAndChatId() {
        return this.getFullName() + " chat id [ " + this.chatId + " ]";
    }
}
