import sys;

import pdfkit;

path_wkthmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
config = pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)
file_out = sys.argv[1];
url = sys.argv[2];
print("Location of out file is ", url);
pdfkit.from_url(url, file_out, configuration=config);
